# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-writemail

CONFIG += sailfishapp

SOURCES += src/harbour-writemail.cpp

OTHER_FILES += qml/harbour-writemail.qml \
    rpm/harbour-writemail.changes \
    rpm/harbour-writemail.yaml \
    harbour-writemail.desktop
