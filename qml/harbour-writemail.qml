/*
    WriteMail - Small app for writing mail to specific address in SailfishOS.
    Copyright (C) 2016  Jiri Hubacek
    Contact: Jiri Hubacek <jiri.hubacek@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import org.nemomobile.email 0.1
import com.jolla.email 1.1

import org.nemomobile.configuration 1.0

ApplicationWindow
{
    allowedOrientations: Orientation.All
    _defaultPageOrientations: Orientation.All

    initialPage: Component {
        Page {
            EmailComposer {
                id: composer
                isEmailApp: true
                discardDraft: true

                emailTo: sendToConfig.value
            }

            EmailAgent {
                id: agent
            }

            Connections {
                target: agent

                onSendCompleted: {
                    Qt.quit()
                }
            }

            ConfigurationValue {
                id: sendToConfig

                key: "/apps/harbour-writemail/settings/send_to"
            }
        }
    }
}
