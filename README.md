This repo is deprecated due to [mailself][] patch. See [this thread at TJC][].

[mailself]: https://coderus.openrepos.net/pm2/project/mailself
[this thread at TJC]: https://together.jolla.com/question/185138/mail-to-self-with-one-button-or-string-in-terminal/

# harbour-writemail
Write mail to specific address shortcut for SailfishOS.

I use my email as [ZTD] inbox so if I have an idea, I need to send an email to
myself quickly. There is no shortcut for sending email to predefined address in
[SailfishOS] - or I haven't found one at least. This is where this app comes
into play. I have a shortcut of [WriteMail] as first in my lock screen apps.

[ZTD]: https://zenhabits.net/zen-to-done-ztd-the-ultimate-simple-productivity-system/
[SailfishOS]: https://sailfishos.org/
[WriteMail]: https://github.com/qeef/harbour-writemail

## Installation
1. Download this app from `bin` folder of this repository ([link](https://github.com/qeef/harbour-writemail/blob/master/bin/harbour-writemail-0.1-1.armv7hl.rpm)) to your device.

2. Install it by click.

3. To make it work properly, it needs to be included in `Settings`->`Apps`:
  1. Connect to your device via ssh.
  2. Change to root by `devel-su`.
  3. Create folder `mkdir /usr/share/jolla-settings/pages/harbour-writemail/`.
  4. Copy `settings/harbour-writemail.json` to
     `/usr/share/jolla-settings/entries/` folder.
  5. Copy `settings/harbour-writemail.qml` to
     `/usr/share/jolla-settings/pages/harbour-writemail/` folder.

```
md5sum: 922f8f8c43638c409993c553904dd71c  harbour-writemail-0.1-1.armv7hl.rpm
```
