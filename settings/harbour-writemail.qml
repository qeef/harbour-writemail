/*
    WriteMail - Small app for writing mail to specific address in SailfishOS.
    Copyright (C) 2016  Jiri Hubacek
    Contact: Jiri Hubacek <jiri.hubacek@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
    This file needs to be copied to
    `/usr/share/jolla-settings/pages/harbour-writemail/` folder.
*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import org.nemomobile.configuration 1.0

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: cont.height

        Column {
            id: cont

            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: "WriteMail"
            }

            TextField {
                id: sendTo

                width: parent.width
                label: "Send To"
                text: (sendToConfig.value == "your.inbox+TDL@whatever.yay") ?
                    "" : 
                    sendToConfig.value

                placeholderText: "Type recipient email address"
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly

                onTextChanged: sendToConfig.value = text ||
                    "your.inbox+TDL@whatever.yay"

                // EnterKey.iconSource: "image://theme/icon-m-enter-close"
                EnterKey.onClicked: focus = false
            }
        }
    }

    ConfigurationValue {
      id: sendToConfig

      key: "/apps/harbour-writemail/settings/send_to"
      defaultValue: "your.inbox+TDL@whatever.yay"
    }
}
